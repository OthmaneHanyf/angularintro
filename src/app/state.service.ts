import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class StateService {

  public reusableComponentState : Subject<boolean>;
  private checkBoxState : boolean;


  constructor() {
    this.reusableComponentState = new Subject<boolean>();
    this.checkBoxState = false;
  }

  checkBoxChanged() {
    this.checkBoxState = !this.checkBoxState;
    this.reusableComponentState.next(this.checkBoxState);
  }

}
