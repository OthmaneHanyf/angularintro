import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-reusable',
  templateUrl: './reusable.component.html',
  styleUrls: ['./reusable.component.scss']
})
export class ReusableComponent implements OnInit {

  checkBoxState : boolean = false;

  constructor(private stateService:StateService) { }

  ngOnInit(): void {
    this.stateService.reusableComponentState.subscribe(checkBoxState => {
      this.checkBoxState = checkBoxState;
      console.log(checkBoxState);
    });
  }

  checkBoxChanged() {
    this.stateService.checkBoxChanged();
  }

}
