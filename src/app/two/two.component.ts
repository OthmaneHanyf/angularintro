import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.scss']
})
export class TwoComponent implements OnInit {

  checkBoxStateTwo : boolean = false;

  constructor(public stateService:StateService) {}

  ngOnInit(): void {
    this.stateService.reusableComponentState.subscribe(checkBoxStateTwo => {
      this.checkBoxStateTwo = checkBoxStateTwo;
    });
  }

}
